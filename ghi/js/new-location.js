
window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/states/';
  try {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error("Bad response!")
    } else {
      const data = await response.json();

      const selectTag = document.getElementById('state');

      for (let state of data.states) {
        const categorySelect = document.querySelector('#state');
        const option = document.createElement('option')
        option.value = state.abbreviation;
        option.innerHTML = state.name;
        categorySelect.appendChild(option);
      }
    }
  } catch (statesError) {
    console.log('There was an error fetching the states:', statesError);
  }

  const formTag = document.getElementById('create-location-form');
  formTag.addEventListener('submit', async (event) => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newLocation = await response.json();
    }
  });
});
