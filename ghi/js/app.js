function createAlert(alertMessage) {
  return `
    <div class="alert alert-danger" role="alert" style="margin: 24px auto">
      ${alertMessage}
    </div>
  `;
}

function createCard(name, description, pictureUrl, startDate, endDate, cardSubtitle) {
    return `
      <div class="card" style="box-shadow: 5px 5px 5px 5px rgba(0,0,0,0.2); margin: 8px 0">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${cardSubtitle}</h6>
          <p class="card-text">${description}</p>
        </div>
        <p class="cardtext">${startDate} - ${endDate}</p>
      </div>
    `;
  }


  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    const row = document.querySelector('.row');

    try {
      const response = await fetch(url);

      if (!response.ok) {
        const html = createAlert('We got a bad response!');
        row.innerHTML += html;
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const cardSubtitle = details.conference.location.name;

            const startDateOne = new Date(details.conference.starts);
            const startDate = startDateOne.toLocaleDateString();
            const endDateOne = new Date(details.conference.ends);
            const endDate = endDateOne.toLocaleDateString();

            const html = createCard(name, description, pictureUrl, startDate, endDate, cardSubtitle);
            row.innerHTML += html;
          }
        }
      }
    } catch (e) {
      const html = createAlert(e);
      row.innerHTML += html;
    }

  });
