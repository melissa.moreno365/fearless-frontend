
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    try {
      const response = await fetch(url);
      if (!response.ok) {
        throw new Error("Bad response!")
      } else {
        const data = await response.json();

        const categorySelect = document.querySelector('#location');

        for (let location of data.locations) {
          const option = document.createElement('option')
          option.value = location.id;
          option.innerHTML = location.name;
          categorySelect.appendChild(option);
        }
      }
    } catch (locationsError) {
      console.log('There was an error fetching the conference:', locationsError);
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async (event) => {
      console.log('~~~ form submits')
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      console.log('~~~ json: ', json);

      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newConference = await response.json();
        console.log('~~~ newConference: ', newConference);
      }
    });
  });
